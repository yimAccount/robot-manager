<?php

namespace yimOHG\RobotManager\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use Docker\Docker;
use Docker\DockerClient;
use Docker\API\Model\ExecConfig;
use Docker\API\Model\ExecStartConfig;

class RobotController extends Controller
{
    public function showTestcases() {
    	$files 			= $this->recursiveDirectoryIterator( 
    		config('robotmanager.case_path') 
    	);

    	$collection 	= collect($files);
    	$filtered 		= $collection->filter(function ($value, $key) {
		    return $value->getExtension() == config('robotmanager.extension') ;
		});    	
    	
        return view('robotmanager::testcases', ['testcases' => $filtered]);
    }

    public function executeTest($filename=null) {

    	try {
	    	$client = new DockerClient([
                'remote_socket' => env('DOCKER_HOST', 'unix:///var/run/docker.sock'),
                'ssl'           => env('DOCKER_SSL', false),
            ]);
	        $docker = new Docker($client);

	        $allContainers = $docker->getContainerManager()->findAll();

	        $collection 	= collect($allContainers);

	        $rfw_container = $collection->filter(function ($value, $key) {
			    return ends_with(
			    		$value->getNames()[0], 
			    		config('robotmanager.container') 
			    	);
			});  

	        $execconfig = new ExecConfig();
	        $execconfig->setCmd(
	        	array(
	        		'pybot', 
	        		'-d', 
	        		config('robotmanager.log_path').'/'.$filename.'/'.time(), 
	        		config('robotmanager.case_path').'/'.$filename.'.'.config('robotmanager.extension') 
	        	)
	        );

	        $startConfig = new ExecStartConfig();
	        $startConfig->setDetach(true);

	        $execmanager = $docker->getExecManager();
	        $response = $execmanager->create($rfw_container->first()->getId(),$execconfig);

	        $responseid = $response->getId();


	        $find = $execmanager->find($responseid);
	        $start = $execmanager->start($responseid, $startConfig);


	    } catch( \Exception $e) {
	        debug($e);
	    }
    	
    	return redirect()->route('robot.testing');
    	//return view('welcome');	
    }

    public function showLog($filename=null) {
    	$files 			= $this->recursiveDirectoryIterator( 
    		config('robotmanager.log_path').'/'.$filename
    	);

    	$collection 	= collect($files);
    	$filtered 		= $collection->filter(function ($value, $key) {
		    return ( $value->isDir() && $value->getFilename() != "." && $value->getFilename() != ".." );
		}); 

    	//debug($filtered);

    	$larafiles = collect(
    		\File::allFiles( config('robotmanager.log_path').'/'.$filename ) 
    	);
    	

    	//debug($larafiles->groupBy('relativePath'));

    	$grouped = $larafiles->groupBy(function ($item, $key) {
		    return $item->getRelativePath();
		});

		//debug($grouped);

    	return view('robotmanager::filetree', ['lists' => $grouped]);
    }

    public function showdetails($file=null)
    {
    	
    	try {
    		$file = base64_decode($file);
	    	$contents = \File::get($file);
	    	
	    	
	    } catch( \Exception $e) {
	    	debug($e);
	    	$contents = "Something went wrong.";
    	}


    	return view('robotmanager::logdetails', ['html' => $contents]);
    }

    public function recursiveDirectoryIterator($directory = null) {
    	$objects = new \RecursiveIteratorIterator(
			new \RecursiveDirectoryIterator($directory), 
			\RecursiveIteratorIterator::SELF_FIRST
		);

    	$files = [];

		foreach ($objects as $name => $object) {
			$files[] = $object;
		}
    
    	return $files;
	}
}
